import { Component } from '@angular/core';
import { User } from '../shared/user.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  users: User[] = [
    new User('Aidai', 'aidai@hawaii.edu', 'true', 'editor'),
    new User( 'Aslam', 'aidai@hawaii.edu', 'false', 'editor'),
  ];
  onNewUser(user:User){
    this.users.push(user);
  }

}
