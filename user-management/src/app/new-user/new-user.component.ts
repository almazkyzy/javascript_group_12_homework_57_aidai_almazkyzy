import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { User } from '../../shared/user.model';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {
  @Output() newUser = new EventEmitter<User>();
  @ViewChild('nameInput') nameInput!: ElementRef;
  @ViewChild('emailInput') emailInput!: ElementRef;
  status: string = '';
  role: string= '';

  addUser(){
    const name = this.nameInput.nativeElement.value;
    const email =this.emailInput.nativeElement.value;
    const status= this.status;
    const role = this.role;

    const userCreated = new User(name, email,status, role);
    this.newUser.emit(userCreated);
    console.log(userCreated);
  }
  constructor() { }

  ngOnInit(): void {
  }

}
